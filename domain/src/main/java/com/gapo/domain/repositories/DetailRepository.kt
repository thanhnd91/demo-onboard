package com.gapo.domain.repositories

import com.gapo.domain.model.detail.Detail
import com.gapo.domain.model.newsfeed.NewsFeed
import io.reactivex.Single

interface DetailRepository {

    fun getDetail(): Single<Detail>
}