package com.gapo.domain.repositories

import com.gapo.domain.model.newsfeed.ItemFeed
import io.reactivex.Single

interface NewsFeedRepository {
    fun getNewsFeed(): Single<ArrayList<ItemFeed>>
}