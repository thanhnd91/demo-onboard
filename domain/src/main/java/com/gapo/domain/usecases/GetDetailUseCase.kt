package com.gapo.domain.usecases

import com.gapo.domain.repositories.DetailRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class GetDetailUseCase: KoinComponent {
    private val detailRepository: DetailRepository by inject()
    operator fun invoke() = detailRepository.getDetail()
}
