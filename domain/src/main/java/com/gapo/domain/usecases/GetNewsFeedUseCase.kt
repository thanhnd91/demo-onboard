package com.gapo.domain.usecases

import com.gapo.domain.repositories.NewsFeedRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class GetNewsFeedUseCase: KoinComponent {
    private val newsFeedRepository: NewsFeedRepository by inject()
    operator fun invoke() = newsFeedRepository.getNewsFeed()
}
