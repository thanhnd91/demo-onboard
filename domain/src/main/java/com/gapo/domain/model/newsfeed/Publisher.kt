package com.gapo.domain.model.newsfeed


data class Publisher(
    var icon: String = "",
    var id: String = "",
    var name: String = ""
)