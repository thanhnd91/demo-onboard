package com.gapo.domain.model.newsfeed

data class Image(
    var height: Int ?= 0,
    var href: String ?= "",
    var mainColor: String ?= "",
    var width: Int ?= 0
)
