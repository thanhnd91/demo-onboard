package com.gapo.domain.model.newsfeed

data class Content(
    var duration: Int? = 0,
    var href: String ?= "",
    var previewImage: Image ?= Image()
)