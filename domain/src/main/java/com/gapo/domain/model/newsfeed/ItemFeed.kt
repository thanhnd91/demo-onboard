package com.gapo.domain.model.newsfeed

import java.util.*
import kotlin.collections.ArrayList

data class ItemFeed(
    var avatar: Image? = Image(),
    var content: Content? = Content(),
    var contentType: String = "",
    var description: String = "",
    var documentId: String = "",
    var images: ArrayList<Image>? = ArrayList(),
    var originUrl: String = "",
    var publishedDate: Date = Date(),
    var publisher: Publisher = Publisher(),
    var title: String = ""
)