package com.gapo.domain.model.detail

data class Content(
    var markups: ArrayList<Markup> = ArrayList(),
    var text: String = ""
)