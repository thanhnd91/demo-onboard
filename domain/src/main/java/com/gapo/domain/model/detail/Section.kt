package com.gapo.domain.model.detail

data class Section(
    var content: Content = Content(),
    var sectionType: Int = 0
)