package com.gapo.domain.model.detail


data class Markup(
    var end: Int = 0,
    var markupType: Int = 0,
    var start: Int = 0
)