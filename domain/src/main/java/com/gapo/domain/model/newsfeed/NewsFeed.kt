package com.gapo.domain.model.newsfeed

data class NewsFeed(
    var items: ArrayList<ItemFeed> = ArrayList()
)