package com.gapo.domain.model.detail

import com.gapo.domain.model.newsfeed.Publisher

data class Detail(
    var description: String = "",
    var documentId: String = "",
    var originUrl: String = "",
    var publishedDate: String = "",
    var publisher: Publisher = Publisher(),
    var sections: ArrayList<Section> = ArrayList(),
    var templateType: String = "",
    var title: String = ""
)