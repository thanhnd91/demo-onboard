package com.gapo.data.database

import com.gapo.data.database.db.NewsFeedDatabase
import com.gapo.data.database.model.newsfeed.CachedItemFeed
import com.gapo.domain.model.newsfeed.ItemFeed
import com.gapo.domain.model.newsfeed.NewsFeed
import com.gapo.data.mapper.local.NewsFeedMapperLocal
import io.reactivex.Single


class NewsFeedLocalDatabase(private val newsFeedDatabase: NewsFeedDatabase) {
    private val newsFeedMapperLocal: NewsFeedMapperLocal = NewsFeedMapperLocal()

    fun getNewsFeedLocal(): Single<ArrayList<ItemFeed>> {
        return newsFeedDatabase.cachedNewsFeedDao.getNewsFeed()
            .map { newsFeedMapperLocal.transform(it as ArrayList<CachedItemFeed>).items }
    }

    fun updateData(listItemFeed: ArrayList<ItemFeed>)
    {
        val newsFeed = NewsFeed(items = listItemFeed)
        val listFeed = newsFeedMapperLocal.transformToRepository(newsFeed)
        newsFeedDatabase.cachedNewsFeedDao.updateData(listFeed)
    }

}
