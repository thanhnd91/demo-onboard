package com.gapo.data.database.model.newsfeed

import androidx.room.Entity
import androidx.room.PrimaryKey

import com.gapo.data.database.db.constants.NewsFeedConstants
import java.util.*
import kotlin.collections.ArrayList

@Entity(tableName = NewsFeedConstants.TABLE_NAME)
data class CachedItemFeed(
    var avatar: CachedImage? = CachedImage(),
    var content: CachedContent? = CachedContent(),
    var contentType: String = "",
    var description: String = "",
    @PrimaryKey
    var documentId: String = "",
    var images: ArrayList<CachedImage>? = ArrayList(),
    var originUrl: String = "",
    var publishedDate: Date = Date(),
    var publisher: CachedPublisher = CachedPublisher(),
    var title: String = ""
)