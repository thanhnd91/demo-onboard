package com.gapo.data.database.db


import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gapo.data.database.dao.CachedNewsFeedDao
import com.gapo.data.database.model.newsfeed.CachedItemFeed
import com.gapo.data.utils.Converters

@Database(entities = arrayOf(CachedItemFeed::class), version = 1)
@TypeConverters(Converters::class)
abstract class NewsFeedDatabase : RoomDatabase() {
    abstract val cachedNewsFeedDao: CachedNewsFeedDao

}