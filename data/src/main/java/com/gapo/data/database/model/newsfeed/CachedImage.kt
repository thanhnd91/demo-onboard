package com.gapo.data.database.model.newsfeed

import com.gapo.domain.model.newsfeed.Image

data class CachedImage(
    var height: Int ?= 0,
    var href: String ?= "",
    var mainColor: String ?= "",
    var width: Int ?= 0
)
{
    companion object {


        fun getListImageFromListCached(listCachedImage: ArrayList<CachedImage>?):ArrayList<Image>
        {
            val imageList = ArrayList<Image>()
            listCachedImage?.map { imageResponse->
                imageList.add(Image(imageResponse.height, imageResponse.href, imageResponse.mainColor, imageResponse.width))
            }
            return imageList
        }


        fun getListCachedImage(listImageResponse: ArrayList<Image>?):ArrayList<CachedImage>
        {
            val imageList = ArrayList<CachedImage>()
            listImageResponse?.map {image->
                imageList.add(CachedImage(image.height, image.href, image.mainColor, image.width))
            }
            return imageList
        }
    }
}
