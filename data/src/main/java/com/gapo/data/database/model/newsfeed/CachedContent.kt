package com.gapo.data.database.model.newsfeed

data class CachedContent(
    var duration: Int? = 0,
    var href: String ?= "",
    var previewImage: CachedImage ?= CachedImage()
)