package com.gapo.data.database.dao

import androidx.room.*
import com.gapo.data.database.model.newsfeed.CachedItemFeed
import io.reactivex.Completable
import io.reactivex.Single
import com.gapo.data.database.db.constants.NewsFeedConstants.DELETE_ALL_NEWSFEEDS
import com.gapo.data.database.db.constants.NewsFeedConstants.QUERY_NEWSFEEDS

@Dao
interface CachedNewsFeedDao {

    @Query(QUERY_NEWSFEEDS)
    fun getNewsFeed(): Single<List<CachedItemFeed>>

    @Query(DELETE_ALL_NEWSFEEDS)
    fun clearNewsFeed(): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewsFeed(cachedItemFeed: CachedItemFeed): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(userEntities: List<CachedItemFeed>): Array<Long>

    @Transaction
    fun updateData(listCachedItemFeed: List<CachedItemFeed>) {
        clearNewsFeed()
        insertAll(listCachedItemFeed)
    }
}