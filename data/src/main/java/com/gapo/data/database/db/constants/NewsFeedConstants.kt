package com.gapo.data.database.db.constants


object NewsFeedConstants {

    const val TABLE_NAME = "CachedItemFeeds"

    const val QUERY_NEWSFEEDS = "SELECT * FROM" + " " + TABLE_NAME

    const val DELETE_ALL_NEWSFEEDS = "DELETE FROM" + " " + TABLE_NAME

}