package com.gapo.data.database.model.newsfeed


data class CachedPublisher(
    var icon: String = "",
    var id: String = "",
    var name: String = ""
)