package com.gapo.data.service.model.newsfeed


import com.gapo.data.database.model.newsfeed.CachedImage
import com.gapo.domain.model.newsfeed.Image
import com.google.gson.annotations.SerializedName

data class ImageEntity(
    @SerializedName("height")
    var height: Int ?= 0,
    @SerializedName("href")
    var href: String ?= "",
    @SerializedName("main_color")
    var mainColor: String ?= "",
    @SerializedName("width")
    var width: Int ?= 0
)

{
    companion object {
        fun getListImage(listImageEntity: ArrayList<ImageEntity>?):ArrayList<Image>
        {
            val imageList = ArrayList<Image>()
            listImageEntity?.map { imageResponse->
                imageList.add(Image(imageResponse.height, imageResponse.href, imageResponse.mainColor, imageResponse.width))
            }
            return imageList
        }

        fun getListImageResponse(listImageResponse: ArrayList<Image>?):ArrayList<ImageEntity>
        {
            val imageList = ArrayList<ImageEntity>()
            listImageResponse?.map {image->
                imageList.add(ImageEntity(image.height, image.href, image.mainColor, image.width))
            }
            return imageList
        }
    }
}
