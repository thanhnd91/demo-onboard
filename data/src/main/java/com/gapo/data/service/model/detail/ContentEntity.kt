package com.gapo.data.service.model.detail

import com.google.gson.annotations.SerializedName

data class ContentEntity(
    @SerializedName("markups")
    var markups: ArrayList<MarkupEntity> = ArrayList(),
    @SerializedName("text")
    var text: String = ""
)