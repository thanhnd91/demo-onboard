package com.gapo.data.service.model.detail


import com.gapo.domain.model.detail.Content
import com.google.gson.annotations.SerializedName

data class SectionEntity(
    @SerializedName("content")
    var content: Content = Content(),
    @SerializedName("section_type")
    var sectionType: Int = 0
)