package com.gapo.data.service.api

import com.gapo.data.service.model.detail.DetailEntity
import com.gapo.data.service.model.newsfeed.NewsFeedEntity
import com.gapo.data.service.api.ApiEndPoint.GET_DETAIL
import com.gapo.data.service.api.ApiEndPoint.GET_NEWSFEED
import io.reactivex.Single
import retrofit2.http.GET

interface NewsFeedApi {

    @GET(GET_NEWSFEED)
    fun getNewsFeed(): Single<NewsFeedEntity>

    @GET(GET_DETAIL)
    fun getDetail(): Single<DetailEntity>
}
