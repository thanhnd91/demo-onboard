package com.gapo.data.service.model.newsfeed


import com.google.gson.annotations.SerializedName

data class NewsFeedEntity(
    @SerializedName("items")
    var items: ArrayList<ItemFeedEntity>  = ArrayList()
)