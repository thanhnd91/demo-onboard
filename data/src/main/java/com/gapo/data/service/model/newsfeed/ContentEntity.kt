package com.gapo.data.service.model.newsfeed


import com.google.gson.annotations.SerializedName

data class ContentEntity(
    @SerializedName("duration")
    var duration: Int? = 0,
    @SerializedName("href")
    var href: String? = "",
    @SerializedName("preview_image")
    var previewImage: ImageEntity? = ImageEntity()
)