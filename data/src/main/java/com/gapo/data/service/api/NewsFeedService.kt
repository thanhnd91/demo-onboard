package com.gapo.data.service.api

import com.gapo.data.NewsFeedRequestGenerator
import com.gapo.domain.model.detail.Detail
import com.gapo.data.mapper.service.DetailMapperService
import com.gapo.data.mapper.service.NewsFeedMapperService
import com.gapo.domain.model.newsfeed.ItemFeed
import io.reactivex.Single

class NewsFeedService {

    private val api: NewsFeedApi = NewsFeedRequestGenerator().createService(NewsFeedApi::class.java)
    private val newsFeedMapperService: NewsFeedMapperService = NewsFeedMapperService()
    private val detailMapperService: DetailMapperService = DetailMapperService()

    fun getNewsFeed(): Single<ArrayList<ItemFeed>> {
        return api.getNewsFeed().map { newsFeedMapperService.transform(it).items }
    }

    fun getDetail(): Single<Detail> {
        return api.getDetail()
            .map { detailMapperService.transform(it) }
    }
}
