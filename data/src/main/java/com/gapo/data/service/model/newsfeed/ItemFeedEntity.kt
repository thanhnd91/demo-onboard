package com.gapo.data.service.model.newsfeed


import com.gapo.domain.model.newsfeed.Publisher
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class ItemFeedEntity(
    @SerializedName("avatar")
    var avatar: ImageEntity? = ImageEntity(),
    @SerializedName("content")
    var content: ContentEntity? = ContentEntity(),
    @SerializedName("content_type")
    var contentType: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("document_id")
    var documentId: String = "",
    @SerializedName("images")
    var images: ArrayList<ImageEntity>? = ArrayList(),
    @SerializedName("origin_url")
    var originUrl: String = "",
    @SerializedName("published_date")
    var publishedDate: Date = Date(),
    @SerializedName("publisher")
    var publisher: Publisher = Publisher(),
    @SerializedName("title")
    var title: String = ""
)