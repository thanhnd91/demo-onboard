package com.gapo.data.service.model.detail

import com.google.gson.annotations.SerializedName

data class MarkupEntity(
    @SerializedName("end")
    var end: Int = 0,
    @SerializedName("markup_type")
    var markupType: Int = 0,
    @SerializedName("start")
    var start: Int = 0
)