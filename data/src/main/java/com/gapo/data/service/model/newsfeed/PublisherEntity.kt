package com.gapo.data.service.model.newsfeed


import com.google.gson.annotations.SerializedName

data class PublisherEntity(
    @SerializedName("icon")
    var icon: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("name")
    var name: String = ""
)