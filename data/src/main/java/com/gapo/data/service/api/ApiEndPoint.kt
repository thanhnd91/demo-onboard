package com.gapo.data.service.api

object ApiEndPoint {

    const val GET_NEWSFEED = "newsfeed.json"
    const val GET_DETAIL = "detail.json"
}