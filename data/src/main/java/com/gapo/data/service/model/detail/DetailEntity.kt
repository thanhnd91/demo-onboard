package com.gapo.data.service.model.detail

import com.gapo.domain.model.newsfeed.Publisher
import com.google.gson.annotations.SerializedName

data class DetailEntity(
    @SerializedName("description")
    var description: String = "",
    @SerializedName("document_id")
    var documentId: String = "",
    @SerializedName("origin_url")
    var originUrl: String = "",
    @SerializedName("published_date")
    var publishedDate: String = "",
    @SerializedName("publisher")
    var publisher: Publisher = Publisher(),
    @SerializedName("sections")
    var sections: ArrayList<SectionEntity> = ArrayList(),
    @SerializedName("template_type")
    var templateType: String = "",
    @SerializedName("title")
    var title: String = ""
)