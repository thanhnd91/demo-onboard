package com.gapo.data.mapper.service

import com.gapo.data.service.model.detail.DetailEntity
import com.gapo.domain.model.detail.Detail
import com.gapo.domain.model.newsfeed.*
import com.gapo.data.mapper.BaseMapperRepository

open class DetailMapperService : BaseMapperRepository<DetailEntity, Detail> {

    override fun transform(type: DetailEntity): Detail {
        return Detail(
            description = type.description,
            documentId = type.documentId,
            originUrl = type.originUrl,
            publishedDate = type.publishedDate,
            publisher = Publisher(
                icon = type.publisher.icon,
                id = type.publisher.id,
                name = type.publisher.name
            ),
            sections = ArrayList(),
            templateType = type.templateType,
            title = type.title
        )
    }

    override fun transformToRepository(type: Detail): DetailEntity {
        return DetailEntity(
            description = type.description,
            documentId = type.documentId,
            originUrl = type.originUrl,
            publishedDate = type.publishedDate,
            publisher = Publisher(
                icon = type.publisher.icon,
                id = type.publisher.id,
                name = type.publisher.name
            ),
            sections = ArrayList(),
            templateType = type.templateType,
            title = type.title
        )
    }
}
