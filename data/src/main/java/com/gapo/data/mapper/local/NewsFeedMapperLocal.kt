package com.gapo.data.mapper.local

import com.gapo.data.database.model.newsfeed.*
import com.gapo.domain.model.newsfeed.*
import com.gapo.data.mapper.BaseMapperRepository


class NewsFeedMapperLocal : BaseMapperRepository<ArrayList<CachedItemFeed>, NewsFeed> {

    override fun transform(type: ArrayList<CachedItemFeed>): NewsFeed {
        val newsFeed = NewsFeed()
        type.map { itemReponse ->
            newsFeed.items.add(
                ItemFeed(
                    avatar = Image(
                        itemReponse.avatar?.height,
                        itemReponse.avatar?.href,
                        itemReponse.avatar?.mainColor,
                        itemReponse.avatar?.width
                    ),
                    content = Content(
                        itemReponse.content?.duration,
                        itemReponse.content?.href,
                        Image(
                            itemReponse.content?.previewImage?.height,
                            itemReponse.content?.previewImage?.href,
                            itemReponse.content?.previewImage?.mainColor,
                            itemReponse.content?.previewImage?.width
                        )
                    ),
                    contentType = itemReponse.contentType,
                    description = itemReponse.description,
                    documentId = itemReponse.documentId,
                    images = CachedImage.getListImageFromListCached(itemReponse.images),
                    originUrl = itemReponse.originUrl,
                    publishedDate = itemReponse.publishedDate,
                    publisher = Publisher(
                        itemReponse.publisher.icon,
                        itemReponse.publisher.id,
                        itemReponse.publisher.name
                    ),
                    title = itemReponse.title
                )
            )
        }

        return newsFeed
    }

    override fun transformToRepository(type: NewsFeed): ArrayList<CachedItemFeed> {
        val itemList =
            ArrayList<CachedItemFeed>()
        type.items.map { item ->
            itemList.add(
                CachedItemFeed(
                    avatar = CachedImage(
                        item.avatar?.height,
                        item.avatar?.href,
                        item.avatar?.mainColor,
                        item.avatar?.width
                    ),
                    content = CachedContent(
                        item.content?.duration,
                        item.content?.href,
                        CachedImage(
                            item.content?.previewImage?.height,
                            item.content?.previewImage?.href,
                            item.content?.previewImage?.mainColor,
                            item.content?.previewImage?.width
                        )
                    ),
                    contentType = item.contentType,
                    description = item.description,
                    documentId = item.documentId,
                    images = CachedImage.getListCachedImage(item.images),
                    originUrl = item.originUrl,
                    publishedDate = item.publishedDate,
                    publisher = CachedPublisher(
                        item.publisher.icon,
                        item.publisher.id,
                        item.publisher.name
                    ),
                    title = item.title
                )
            )
        }

        return itemList
    }

}
