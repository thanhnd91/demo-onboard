package com.gapo.data.mapper.service

import com.gapo.data.service.model.newsfeed.ContentEntity
import com.gapo.data.service.model.newsfeed.ImageEntity
import com.gapo.data.service.model.newsfeed.ImageEntity.Companion.getListImage
import com.gapo.data.service.model.newsfeed.ImageEntity.Companion.getListImageResponse
import com.gapo.data.service.model.newsfeed.ItemFeedEntity
import com.gapo.data.service.model.newsfeed.NewsFeedEntity
import com.gapo.domain.model.newsfeed.*
import com.gapo.data.mapper.BaseMapperRepository

open class NewsFeedMapperService : BaseMapperRepository<NewsFeedEntity, NewsFeed> {

    override fun transform(type: NewsFeedEntity): NewsFeed {
        val newsFeed = NewsFeed()
        type.items.map { itemReponse ->
            newsFeed.items.add(
                ItemFeed(
                    avatar = Image(
                        itemReponse.avatar?.height,
                        itemReponse.avatar?.href,
                        itemReponse.avatar?.mainColor,
                        itemReponse.avatar?.width
                    ),
                    content = Content(
                        itemReponse.content?.duration,
                        itemReponse.content?.href,
                        Image(
                            itemReponse.content?.previewImage?.height,
                            itemReponse.content?.previewImage?.href,
                            itemReponse.content?.previewImage?.mainColor,
                            itemReponse.content?.previewImage?.width
                        )
                    ),
                    contentType = itemReponse.contentType,
                    description = itemReponse.description,
                    documentId = itemReponse.documentId,
                    images = getListImage(itemReponse.images),
                    originUrl = itemReponse.originUrl,
                    publishedDate = itemReponse.publishedDate,
                    publisher = Publisher(
                        itemReponse.publisher.icon,
                        itemReponse.publisher.id,
                        itemReponse.publisher.name
                    ),
                    title = itemReponse.title
                )
            )
        }

        return newsFeed
    }

    override fun transformToRepository(type: NewsFeed): NewsFeedEntity {
        val newsFeedResponse = NewsFeedEntity()
        type.items.map { item ->
            newsFeedResponse.items.add(
                ItemFeedEntity(
                    avatar = ImageEntity(
                        item.avatar?.height,
                        item.avatar?.href,
                        item.avatar?.mainColor,
                        item.avatar?.width
                    ),
                    content = ContentEntity(
                        item.content?.duration,
                        item.content?.href,
                        ImageEntity(
                            item.content?.previewImage?.height,
                            item.content?.previewImage?.href,
                            item.content?.previewImage?.mainColor,
                            item.content?.previewImage?.width
                        )
                    ),
                    contentType = item.contentType,
                    description = item.description,
                    documentId = item.documentId,
                    images = getListImageResponse(item.images),
                    originUrl = item.originUrl,
                    publishedDate = item.publishedDate,
                    publisher = Publisher(
                        item.publisher.icon,
                        item.publisher.id,
                        item.publisher.name
                    ),
                    title = item.title
                )
            )
        }

        return newsFeedResponse
    }
}
