package com.gapo.data.utils

import androidx.room.TypeConverter
import com.gapo.data.database.model.newsfeed.CachedContent
import com.gapo.data.database.model.newsfeed.CachedImage
import com.gapo.data.database.model.newsfeed.CachedPublisher
import com.google.gson.Gson
import java.util.*


class Converters {

    @TypeConverter
    fun listCachedImageToJson(value: ArrayList<CachedImage>?): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToListCachedImage(value: String): ArrayList<CachedImage>? {

        return Gson().fromJson(value, Array<CachedImage>::class.java) as ArrayList<CachedImage>
    }

    @TypeConverter
    fun cachedImageToString(cachedImage: CachedImage): String = Gson().toJson(cachedImage)

    @TypeConverter
    fun stringToCachedImage(string: String): CachedImage = Gson().fromJson(string, CachedImage::class.java)


    @TypeConverter
    fun cachedContentToString(cachedContent: CachedContent): String = Gson().toJson(cachedContent)

    @TypeConverter
    fun stringToCachedContent(string: String): CachedContent = Gson().fromJson(string, CachedContent::class.java)

    @TypeConverter
    fun cachedPublisherToString(cachedPublisher: CachedPublisher): String = Gson().toJson(cachedPublisher)

    @TypeConverter
    fun stringToCachedPublisher(string: String): CachedPublisher = Gson().fromJson(string, CachedPublisher::class.java)

    @TypeConverter
    fun toDate(dateLong: Long): Date {
        return Date(dateLong)
    }

    @TypeConverter
    fun fromDate(date: Date): Long {
        return (date.time)
    }
}