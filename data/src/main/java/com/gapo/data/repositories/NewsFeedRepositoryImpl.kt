package com.gapo.data.repositories

import com.gapo.data.database.NewsFeedLocalDatabase
import com.gapo.data.service.api.NewsFeedService
import com.gapo.domain.model.newsfeed.ItemFeed
import com.gapo.domain.repositories.NewsFeedRepository
import io.reactivex.Single

class NewsFeedRepositoryImpl(
    private val newsFeedService: NewsFeedService,
    private val newsFeedLocalDatabase: NewsFeedLocalDatabase
) : NewsFeedRepository {

    override fun getNewsFeed(): Single<ArrayList<ItemFeed>> {
        return fetchNewsFeed()
    }

    private fun getNewsFeedFromLocal(): Single<ArrayList<ItemFeed>>
    {
       return newsFeedLocalDatabase.getNewsFeedLocal()
    }

    private fun fetchNewsFeed(): Single<ArrayList<ItemFeed>> {
        return newsFeedService.getNewsFeed()
        //.observeOn(AndroidSchedulers.mainThread())
        //.doOnSuccess { newsFeedLocalDatabase.updateData(it) }
    }

}
