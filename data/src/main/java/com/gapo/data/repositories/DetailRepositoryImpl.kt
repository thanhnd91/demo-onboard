package com.gapo.data.repositories

import com.gapo.data.service.api.NewsFeedService
import com.gapo.domain.model.detail.Detail
import com.gapo.domain.repositories.DetailRepository
import io.reactivex.Single

class DetailRepositoryImpl(
    private val newsFeedService: NewsFeedService
) : DetailRepository {

    override fun getDetail(): Single<Detail> {
        return newsFeedService.getDetail()
    }
}
