package com.gapo.data

import com.gapo.data.utils.Constant
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


private const val NEWS_FEED_BASE_URL = "https://raw.githubusercontent.com/Akaizz/static/master/"

private const val TIME_TIMEOUT = 120L

class NewsFeedRequestGenerator {

    private val httpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        })
        .connectTimeout(TIME_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(TIME_TIMEOUT, TimeUnit.SECONDS)


    private val builder = Retrofit.Builder()
        .baseUrl(NEWS_FEED_BASE_URL)
        .addConverterFactory(
            GsonConverterFactory.create(
                GsonBuilder()
                    .setDateFormat(Constant.DATE_FORMAT)
                    .create()
            )
        )
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

    fun <S> createService(serviceClass: Class<S>): S {
        val retrofit = builder.client(httpClient.build()).build()
        return retrofit.create(serviceClass)
    }
}
