package com.airbnb.epoxy.test;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.SnapHelper;

import com.airbnb.epoxy.Carousel;
import com.airbnb.epoxy.Carousel.SnapHelperFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class CarouselTest {

    @Test
    public void testOverrideGlobalSnapHelper() {
        Carousel.setDefaultGlobalSnapHelperFactory(new SnapHelperFactory() {
            @NonNull
            @Override
            public SnapHelper buildSnapHelper(Context context) {
                return new LinearSnapHelper();
            }
        });
    }

    @Test
    public void testODisableGlobalSnapHelper() {
        Carousel.setDefaultGlobalSnapHelperFactory(null);
    }
}
