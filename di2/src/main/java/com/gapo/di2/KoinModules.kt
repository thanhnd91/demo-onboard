package com.gapo.di2

import com.gapo.data.database.NewsFeedLocalDatabase
import com.gapo.data.repositories.DetailRepositoryImpl
import com.gapo.data.repositories.NewsFeedRepositoryImpl
import com.gapo.data.service.api.NewsFeedService
import com.gapo.domain.repositories.DetailRepository
import com.gapo.domain.repositories.NewsFeedRepository
import org.koin.dsl.module

val repositoriesModule = module {
    single { NewsFeedService() }
    single { NewsFeedLocalDatabase(get()) }
    single<DetailRepository> { DetailRepositoryImpl(get()) }
    single<NewsFeedRepository> { NewsFeedRepositoryImpl(get(),get()) }
}
