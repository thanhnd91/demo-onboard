package com.gapo.demoonboard

import android.app.Application
import com.gapo.demoonboard.di.databaseModule
import com.gapo.demoonboard.di.useCasesModule
import com.gapo.demoonboard.di.viewModelsModule
import com.gapo.di2.repositoriesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DemoOnBoardApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
           androidContext(this@DemoOnBoardApplication)
            modules(listOf(databaseModule, repositoriesModule, viewModelsModule, useCasesModule))
      }

    }
}
