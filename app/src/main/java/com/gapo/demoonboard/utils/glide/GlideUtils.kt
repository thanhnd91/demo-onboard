package com.gapo.demoonboard.utils.glide


import com.bumptech.glide.MemoryCategory
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.gapo.demoonboard.ui.newsfeed.enum.ImageType
import com.gapo.demoonboard.utils.RADIUS_IMAGE
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

class GlideUtils {

    companion object {

        fun getMemoryCategory(isHighPerformingDevice: Boolean): MemoryCategory {
            return if (isHighPerformingDevice) {
                MemoryCategory.NORMAL
            } else {
                MemoryCategory.LOW
            }
        }

        fun getDefaultRequestOptions(isHighPerformingDevice: Boolean): RequestOptions {
            return if (isHighPerformingDevice) {
                RequestOptions().format(DecodeFormat.PREFER_ARGB_8888)
            } else {
                RequestOptions().format(DecodeFormat.PREFER_RGB_565)
            }
        }

        fun getRequestOptions(): RequestOptions {
            return RequestOptions()
                .centerCrop()
                .transforms(
                    CenterCrop(), RoundedCorners(RADIUS_IMAGE)
                )
        }

        fun getRequestOptionsGalleryLinear(type: Int, position: Int): RequestOptions {
            var cornerType = RoundedCornersTransformation.CornerType.ALL
            var radius = RADIUS_IMAGE
            when (type) {

                ImageType.TYPE_TWO.type -> {
                    when (position) {
                        0 -> {
                            cornerType = RoundedCornersTransformation.CornerType.TOP
                        }
                        1 -> {
                            cornerType = RoundedCornersTransformation.CornerType.BOTTOM
                        }

                    }
                }

                ImageType.TYPE_THREE.type -> {
                    when (position) {
                        0 -> {
                            cornerType = RoundedCornersTransformation.CornerType.LEFT
                        }
                        1 -> {
                            cornerType = RoundedCornersTransformation.CornerType.TOP_RIGHT
                        }
                        2 -> {
                            cornerType = RoundedCornersTransformation.CornerType.BOTTOM_RIGHT
                        }

                    }
                }

            }
            return RequestOptions()
                .centerCrop()
                .transforms(
                    CenterCrop(), RoundedCornersTransformation(
                        radius, 0,
                        cornerType
                    )
                )
        }

        fun getRequestOptionsGallery(type: Int, position: Int): RequestOptions {
            var cornerType = RoundedCornersTransformation.CornerType.ALL
            var radius = RADIUS_IMAGE
            when (type) {
                ImageType.TYPE_FOUR.type -> when (position) {
                    0 -> {
                        cornerType = RoundedCornersTransformation.CornerType.TOP_LEFT
                    }
                    1 -> {
                        cornerType = RoundedCornersTransformation.CornerType.TOP_RIGHT
                    }
                    2 -> {
                        cornerType = RoundedCornersTransformation.CornerType.BOTTOM_LEFT
                    }
                    3 -> {
                        cornerType = RoundedCornersTransformation.CornerType.BOTTOM_RIGHT
                    }
                }

                ImageType.TYPE_TWO.type -> {
                    when (position) {
                        0 -> {
                            cornerType = RoundedCornersTransformation.CornerType.LEFT
                        }
                        1 -> {
                            cornerType = RoundedCornersTransformation.CornerType.RIGHT
                        }

                    }
                }
                ImageType.TYPE_ONE.type -> {
                    cornerType = RoundedCornersTransformation.CornerType.ALL
                }

                ImageType.TYPE_THREE.type -> when (position) {
                    0 -> {
                        cornerType = RoundedCornersTransformation.CornerType.TOP
                    }
                    1 -> {
                        cornerType = RoundedCornersTransformation.CornerType.BOTTOM_LEFT
                    }
                    2 -> {
                        cornerType = RoundedCornersTransformation.CornerType.BOTTOM_RIGHT
                    }

                }

                ImageType.TYPE_FIVE.type -> when (position) {
                    0 -> {
                        cornerType = RoundedCornersTransformation.CornerType.TOP_LEFT
                    }
                    1 -> {
                        cornerType = RoundedCornersTransformation.CornerType.TOP_RIGHT
                    }
                    2 -> {
                        cornerType = RoundedCornersTransformation.CornerType.BOTTOM_LEFT
                    }
                    3 -> {
                        radius = 0
                    }
                    4 -> {
                        cornerType = RoundedCornersTransformation.CornerType.BOTTOM_RIGHT
                    }

                }

            }
            return RequestOptions()
                .centerCrop()
                .transforms(
                    CenterCrop(), RoundedCornersTransformation(
                        radius, 0,
                        cornerType
                    )
                )
        }
    }
}

