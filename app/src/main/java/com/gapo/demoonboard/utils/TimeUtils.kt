package com.gapo.demoonboard.utils


import java.util.concurrent.TimeUnit

class TimeUtils {

    companion object {
        fun getTimeVideo(time: Int?): String {
            val millis = (time ?: 0) * 1000L
            return String.format(
                "%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(
                    TimeUnit.MILLISECONDS.toHours(
                        millis
                    )
                )), // The change is in this line
                (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(
                    TimeUnit.MILLISECONDS.toMinutes(
                        millis
                    )
                ))
            )

        }
    }
}

