package com.gapo.demoonboard.utils


import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gapo.demoonboard.R
import com.gapo.demoonboard.ui.newsfeed.enum.ImageShapeType
import com.gapo.demoonboard.ui.newsfeed.enum.ImageType
import com.gapo.domain.model.newsfeed.Image

class RecyclerViewUtils {

    companion object {
        fun getLayOutManager(
            galleryList: List<Image>,
            context: Context
        ): RecyclerView.LayoutManager {
            return when (getImageType(galleryList.size)) {
                ImageType.TYPE_ONE.type -> {
                    return LinearLayoutManager(context)
                }
                ImageType.TYPE_TWO.type -> {
                    val orientation = getOrientationForListImage(galleryList)
                    if (orientation == GridLayoutManager.HORIZONTAL) {
                        return LinearLayoutManager(context)
                    } else {
                        GridLayoutManager(
                            context,
                            2,
                            orientation,
                            false
                        )
                    }


                }
                ImageType.TYPE_FOUR.type -> {
                    GridLayoutManager(context, 2)
                }
                ImageType.TYPE_THREE.type -> {
                    //getGridLayOutManager(context, galleryList.size)
                    /*val gridLayoutManager = GridLayoutManager(context, 2,GridLayoutManager.HORIZONTAL,false)
                    gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                        override fun getSpanSize(position: Int): Int {
                            if (position == 0)
                                return 2
                            return 1
                        }
                    }
                    return gridLayoutManager*/

                    val gridLayoutManager = GridLayoutManager(
                        context, 2,
                        getOrientationForListImage(galleryList), false
                    )
                    gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                        override fun getSpanSize(position: Int): Int {
                            if (position == 0)
                                return 2
                            return 1
                        }
                    }
                    return gridLayoutManager
                }
                else -> {
                    getGridLayOutManager(context, galleryList.size)
                }
            }

        }


        private fun getOrientationForListImage(galleryList: List<Image>): Int {
            return when (getImageType(galleryList.size)) {

                ImageType.TYPE_TWO.type -> {
                    getOrientationForTwoImage(galleryList)
                }

                ImageType.TYPE_THREE.type -> {
                    getOrientationForThreeImage(galleryList)
                }
                else -> {
                    GridLayoutManager.VERTICAL
                }
            }
        }

        fun getOrientationForThreeImage(galleryList: List<Image>): Int {
            return if (getImageShapeType(
                    galleryList[0].width!!,
                    galleryList[0].height!!
                ) == ImageShapeType.LANDSCAPE
            )
                GridLayoutManager.VERTICAL
            else
                GridLayoutManager.HORIZONTAL
        }


        private fun getOrientationForTwoImage(galleryList: List<Image>): Int {
            return if (getIsLinearLayOutManager(galleryList))
                GridLayoutManager.HORIZONTAL
            else
                GridLayoutManager.VERTICAL
        }

        fun getImageShapeType(galleryList: List<Image>): ImageShapeType {
            var countLandScape = 0
            var countPortrait = 0
            galleryList.map {
                if (getImageShapeType(it.width!!, it.height!!) == ImageShapeType.LANDSCAPE)
                    countLandScape++
                if (getImageShapeType(it.width!!, it.height!!) == ImageShapeType.PORTRAIT)
                    countPortrait++
            }
            if (countLandScape == 2)
                return ImageShapeType.LANDSCAPE
            else if (countPortrait == 2)
                return ImageShapeType.PORTRAIT
            return ImageShapeType.SQUARE
        }

        private fun getIsLinearLayOutManager(galleryList: List<Image>): Boolean {
            var countLandScape = 0

            galleryList.map {
                if (getImageShapeType(it.width!!, it.height!!) == ImageShapeType.LANDSCAPE)
                    countLandScape++

            }
            return countLandScape == 2
        }

        private fun getGridLayOutManager(context: Context, size: Int): GridLayoutManager {
            val gridLayoutManager = GridLayoutManager(context, getSpanCount(size))
            gridLayoutManager.spanSizeLookup = getSpanSizeLookup(size)
            return gridLayoutManager
        }

        private fun getSpanSizeLookup(size: Int): GridLayoutManager.SpanSizeLookup {
            if (size == 3) {
                return object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        if (position == 0)
                            return 2
                        return 1
                    }
                }
            }
            return object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    if (position == 0 || position == 1)
                        return 3
                    return 2
                }
            }
        }

        private fun getSpanCount(size: Int): Int {
            if (size == 3) {
                return 2
            }
            return 6
        }


        private fun getImageType(size: Int): Int {
            return when (size) {
                1 -> {
                    ImageType.TYPE_ONE.type
                }
                2 -> {
                    ImageType.TYPE_TWO.type
                }
                4 -> {
                    ImageType.TYPE_FOUR.type
                }
                3 -> {
                    ImageType.TYPE_THREE.type
                }
                else -> {
                    ImageType.TYPE_FIVE.type
                }
            }
        }

        fun getImageShapeType(width: Int, height: Int): ImageShapeType {
            if (width > height)
                return ImageShapeType.LANDSCAPE
            else if (width < height)
                return ImageShapeType.PORTRAIT
            return ImageShapeType.SQUARE
        }


        fun getItemDecoratorVertical(context: Context): DividerItemDecoration {
            val itemDecoratorVertical =
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            ContextCompat.getDrawable(
                context,
                R.drawable.divider_vertical
            )?.let { itemDecoratorVertical.setDrawable(it) }
            return itemDecoratorVertical
        }

        fun getItemDecoratorHorizontal(context: Context): DividerItemDecoration {
            val itemDecoratorHorizontal =
                DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL)
            ContextCompat.getDrawable(
                context,
                R.drawable.divider_horizontal
            )?.let { itemDecoratorHorizontal.setDrawable(it) }
            return itemDecoratorHorizontal
        }


    }
}
