package com.gapo.demoonboard.utils


import android.app.Activity
import android.content.Context
import android.text.format.DateUtils
import android.util.DisplayMetrics
import android.util.TypedValue
import com.gapo.demoonboard.R
import com.gapo.domain.model.newsfeed.Image
import com.gapo.domain.model.newsfeed.ItemFeed

var widthScreen = 0

fun getCategoryList(context: Context): List<String> {
    return context.resources.getStringArray(R.array.list_category).toList()
}

fun getPublisherInfo(itemFeed: ItemFeed): String {

    return "${itemFeed.publisher.name} * ${DateUtils.getRelativeTimeSpanString(itemFeed.publishedDate.time)}"
}

fun getRandomImage(images: List<Image>): Image? {
    if (images.isNotEmpty())
        return images.shuffled().take(1)[0]
    return null
}

fun getRemainingImage(size: Int, currentPosition: Int): String {
    return "+${size - currentPosition - 1}"
}

fun getWidthNewsFeedScreen(activity: Activity): Int {
    val displayMetrics = DisplayMetrics()
    activity.windowManager.defaultDisplay.getMetrics(displayMetrics)

    return displayMetrics.widthPixels
}

fun getPixelSizeFromDp(size: Float, context: Context): Int {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, size,
        context.resources.displayMetrics
    ).toInt()
}

