package com.gapo.demoonboard.utils


import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import java.util.*

class LocaleUtils {

    companion object {
        fun updateBaseContextLocale(context: Context): Context {

            val locale = Locale("vi")
            Locale.setDefault(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return updateResourcesLocale(context, locale)
            }
            return updateResourcesLocaleLegacy(context, locale)
        }

        @TargetApi(Build.VERSION_CODES.N)
        private fun updateResourcesLocale(context: Context, locale: Locale): Context {
            val configuration = context.resources.configuration
            configuration.setLocale(locale)
            return context.createConfigurationContext(configuration)
        }

        private fun updateResourcesLocaleLegacy(context: Context, locale: Locale): Context {
            val resources = context.resources
            val configuration = resources.configuration
            configuration.locale = locale
            resources.updateConfiguration(configuration, resources.displayMetrics)
            return context
        }
    }
}

