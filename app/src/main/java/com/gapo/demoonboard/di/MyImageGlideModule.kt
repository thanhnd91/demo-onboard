package com.gapo.demoonboard.di

import android.app.ActivityManager
import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.gapo.demoonboard.utils.PerformanceChecker
import com.gapo.demoonboard.utils.glide.GlideUtils.Companion.getDefaultRequestOptions
import com.gapo.demoonboard.utils.glide.GlideUtils.Companion.getMemoryCategory


@GlideModule
class MyImageGlideModule() : AppGlideModule() {
    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        super.registerComponents(context, glide, registry)
        val devicePerformance = PerformanceChecker(
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        )

        glide.setMemoryCategory(getMemoryCategory(devicePerformance.isHighPerformingDevice))
    }

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val devicePerformance = PerformanceChecker(
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        )

        builder.setDefaultRequestOptions(getDefaultRequestOptions(devicePerformance.isHighPerformingDevice))
    }

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }
}
