package com.gapo.demoonboard.di

import android.content.Context
import androidx.room.Room
import com.gapo.data.database.db.NewsFeedDatabase
import com.gapo.demoonboard.viewmodels.NewsFeedViewModel
import com.gapo.domain.usecases.GetDetailUseCase
import com.gapo.domain.usecases.GetNewsFeedUseCase
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val databaseModule = module {

    fun provideDatabase(context: Context): NewsFeedDatabase {
        return Room.databaseBuilder(context, NewsFeedDatabase::class.java, "newsfeed.db")
            .build()
    }

    single { provideDatabase(androidContext()) }
}

val viewModelsModule = module {
    viewModel { NewsFeedViewModel(get()) }
}

val useCasesModule = module {
    single { GetNewsFeedUseCase() }
    single { GetDetailUseCase() }
}