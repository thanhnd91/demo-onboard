package com.gapo.demoonboard.ui.newsfeed.models.feed

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.epoxy.EpoxyVisibilityTracker
import com.gapo.demoonboard.R
import com.gapo.demoonboard.ui.newsfeed.controller.GalleryController
import com.gapo.demoonboard.ui.newsfeed.viewholder.feed.ItemGalleryHolder
import com.gapo.demoonboard.utils.DELAY_TIME
import com.gapo.demoonboard.utils.RecyclerViewUtils.Companion.getItemDecoratorHorizontal
import com.gapo.demoonboard.utils.RecyclerViewUtils.Companion.getItemDecoratorVertical
import com.gapo.demoonboard.utils.RecyclerViewUtils.Companion.getLayOutManager
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.disposebag.disposedBy
import com.gapo.domain.model.newsfeed.Image
import com.jakewharton.rxbinding3.view.clicks
import java.util.concurrent.TimeUnit


@EpoxyModelClass(layout = R.layout.item_gallery)
abstract class ItemGalleryModel : EpoxyModelWithHolder<ItemGalleryHolder>() {

    @EpoxyAttribute
    lateinit var onCloseButtonClickListener: () -> Unit
    @EpoxyAttribute
    lateinit var title: String
    @EpoxyAttribute
    lateinit var publisherInfo: String

    @EpoxyAttribute
    var galleryList: List<Image>? = null
    @EpoxyAttribute
    lateinit var bag: DisposeBag

    override fun bind(holder: ItemGalleryHolder) {
        holder.tvTitle.text = title
        holder.tvPublisherInfo.text = publisherInfo
        handleClickEvent(holder)
        setUpGallery(holder)
    }

    private fun handleClickEvent(holder: ItemGalleryHolder) {
        holder.clClose.clicks()
            .throttleFirst(DELAY_TIME, TimeUnit.MILLISECONDS)
            .subscribe {
                onCloseButtonClickListener()
            }
            .disposedBy(bag)
    }

    private fun setUpGallery(holder: ItemGalleryHolder) {
        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(holder.rvGallery)

        val controller = GalleryController(bag)
        controller.setData(galleryList)
        holder.rvGallery.layoutManager =
            galleryList?.let { getLayOutManager(it, holder.rvGallery.context) }
        holder.rvGallery.adapter = controller.adapter
        if (holder.rvGallery.itemDecorationCount == 0) {
            holder.rvGallery.addItemDecoration(getItemDecoratorVertical(holder.rvGallery.context))
            holder.rvGallery.addItemDecoration(getItemDecoratorHorizontal(holder.rvGallery.context))
        }

    }
}


