package com.gapo.demoonboard.ui.newsfeed.models.gallery.morefiveimage

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gapo.demoonboard.R
import com.gapo.demoonboard.di.GlideApp
import com.gapo.demoonboard.ui.newsfeed.enum.ImageType
import com.gapo.demoonboard.ui.newsfeed.viewholder.gallery.ItemMoreFiveImageHolder
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.glide.GlideUtils.Companion.getRequestOptionsGallery
import com.gapo.demoonboard.utils.glide.MyImageRequestListener
import com.gapo.domain.model.newsfeed.Image


@EpoxyModelClass(layout = R.layout.item_more_five_image)
abstract class ItemMoreFiveImageModel : EpoxyModelWithHolder<ItemMoreFiveImageHolder>() {

    @EpoxyAttribute
    var imageGallery: Image? = null
    @EpoxyAttribute
    lateinit var bag: DisposeBag
    @EpoxyAttribute
    lateinit var countRemaining: String
    @EpoxyAttribute
    var position: Int = 0

    override fun bind(holder: ItemMoreFiveImageHolder) {
        holder.tvCount.text = countRemaining
        loadImage(holder)
    }


    private fun loadImage(holder: ItemMoreFiveImageHolder) {
        val requestOption = getRequestOptionsGallery(
            ImageType.TYPE_FIVE.type,
            position
        )

        if (imageGallery != null)
            GlideApp.with(holder.ivFive.context).load(imageGallery?.href)
                .transition(DrawableTransitionOptions.withCrossFade())
                .thumbnail(
                    GlideApp.with(holder.ivFive.context)
                        .load(imageGallery?.href)
                        .apply(requestOption)
                )
                .apply(requestOption)
                .listener(MyImageRequestListener())
                .into(holder.ivFive)
    }
}


