package com.gapo.demoonboard.ui.newsfeed.enum

enum class ContentType(val type: String) {
    OVERVIEW("overview"),
    STORY("story"),
    GALLERY("gallery"),
    VIDEO("video"),
    ARTICLE("article"),
    LONG_FORM("long_form")
}