package com.gapo.demoonboard.ui.newsfeed.viewholder.gallery

import android.widget.ImageView
import com.gapo.demoonboard.R
import com.gapo.demoonboard.utils.epoxy.KotlinEpoxyHolder

class ItemImageSquareHolder : KotlinEpoxyHolder() {
    val ivSquare by bind<ImageView>(R.id.ivSquare)
}