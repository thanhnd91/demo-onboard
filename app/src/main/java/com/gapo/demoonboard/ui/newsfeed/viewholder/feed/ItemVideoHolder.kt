package com.gapo.demoonboard.ui.newsfeed.viewholder.feed


import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.media.PlaybackInfo
import com.airbnb.epoxy.toro.ToroUtil
import com.airbnb.epoxy.widget.EthanRecyclerView
import com.gapo.demoonboard.R
import com.gapo.demoonboard.utils.epoxy.KotlinEpoxyPlayerHolder
import com.google.android.exoplayer2.ui.PlayerView
import im.ene.toro.exoplayer.ExoPlayerViewHelper
import im.ene.toro.exoplayer.Playable


class ItemVideoHolder : KotlinEpoxyPlayerHolder() {
    val tvPublisherInfo by bind<TextView>(R.id.tvPublisherInfo)
    val clClose by bind<ConstraintLayout>(R.id.clClose)
    val tvTime by bind<TextView>(R.id.tvTime)
    val ivPreview by bind<ImageView>(R.id.ivPreview)
    val pvVideo by bind<PlayerView>(R.id.pvVideo)
    val parent by bind<ConstraintLayout>(R.id.parent)


    private val listener = object : Playable.DefaultEventListener() {
        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            super.onPlayerStateChanged(playWhenReady, playbackState)
        }
    }


    private var helper: ExoPlayerViewHelper? = null
    private var mediaUri: Uri? = null
    private var position: Int = 1

    override fun getPlayerView(): View {
        return this.pvVideo
    }

    override fun getCurrentPlaybackInfo(): PlaybackInfo {
        return if (helper != null) helper!!.latestPlaybackInfo else PlaybackInfo()
    }

    override fun initialize(container: EthanRecyclerView, playbackInfo: PlaybackInfo) {
        if (mediaUri == null) throw IllegalStateException("mediaUri is null.")
        if (helper == null) {
            helper = ExoPlayerViewHelper(this, mediaUri!!)
            helper!!.addEventListener(listener)
        }
        helper!!.initialize(container, playbackInfo)
    }

    override fun play() {
        if (helper != null) helper!!.play()
    }

    override fun pause() {
        if (helper != null) helper!!.pause()
    }

    override fun isPlaying(): Boolean {
        val isPlay = helper != null && helper!!.isPlaying
        return helper != null && helper!!.isPlaying
    }

    override fun release() {
        if (helper != null) {
            helper!!.removeEventListener(listener)
            helper!!.release()
            helper = null
        }
    }

    override fun wantsToPlay(): Boolean {
        return ToroUtil.visibleAreaOffset(this, parent) >= 0.85
    }

    override fun getPlayerOrder(): Int {
        return position
    }


    fun bindUri(position: Int, uri: Uri?) {
        this.position = position
        mediaUri = uri
    }
}