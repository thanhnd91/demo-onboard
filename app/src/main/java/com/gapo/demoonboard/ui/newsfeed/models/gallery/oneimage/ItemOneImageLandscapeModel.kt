package com.gapo.demoonboard.ui.newsfeed.models.gallery.oneimage

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gapo.demoonboard.R
import com.gapo.demoonboard.di.GlideApp
import com.gapo.demoonboard.ui.newsfeed.enum.ImageType
import com.gapo.demoonboard.ui.newsfeed.viewholder.gallery.ItemImageLandscapeHolder
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.glide.GlideUtils.Companion.getRequestOptionsGallery
import com.gapo.demoonboard.utils.glide.MyImageRequestListener
import com.gapo.domain.model.newsfeed.Image


@EpoxyModelClass(layout = R.layout.item_image_landscape)
abstract class ItemOneImageLandscapeModel : EpoxyModelWithHolder<ItemImageLandscapeHolder>() {

    @EpoxyAttribute
    var imageGallery: Image? = null
    @EpoxyAttribute
    lateinit var bag: DisposeBag
    @EpoxyAttribute
    var position: Int = 0

    override fun bind(holder: ItemImageLandscapeHolder) {
        loadImage(holder)
    }


    private fun loadImage(holder: ItemImageLandscapeHolder) {
        val requestOption = getRequestOptionsGallery(ImageType.TYPE_ONE.type, position)

        if (imageGallery != null)
            GlideApp.with(holder.ivLandscape.context).load(imageGallery?.href)
                .transition(DrawableTransitionOptions.withCrossFade())
                .thumbnail(
                    GlideApp.with(holder.ivLandscape.context)
                        .load(imageGallery?.href)
                        .apply(requestOption)
                )
                .apply(requestOption)
                .listener(MyImageRequestListener())
                .into(holder.ivLandscape)
    }


}


