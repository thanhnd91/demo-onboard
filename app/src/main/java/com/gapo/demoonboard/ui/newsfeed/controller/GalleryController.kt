package com.gapo.demoonboard.ui.newsfeed.controller

import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.TypedEpoxyController
import com.gapo.demoonboard.ui.newsfeed.enum.ImageShapeType
import com.gapo.demoonboard.ui.newsfeed.models.gallery.fiveimage.itemFiveImage
import com.gapo.demoonboard.ui.newsfeed.models.gallery.fourimage.itemFourImage

import com.gapo.demoonboard.ui.newsfeed.models.gallery.morefiveimage.itemMoreFiveImage
import com.gapo.demoonboard.ui.newsfeed.models.gallery.oneimage.itemOneImageLandscape
import com.gapo.demoonboard.ui.newsfeed.models.gallery.oneimage.itemOneImagePortrait
import com.gapo.demoonboard.ui.newsfeed.models.gallery.oneimage.itemOneImageSquare
import com.gapo.demoonboard.ui.newsfeed.models.gallery.threeimage.itemThreeImage
import com.gapo.demoonboard.ui.newsfeed.models.gallery.threeimage.itemThreeImagePortrait
import com.gapo.demoonboard.ui.newsfeed.models.gallery.threeimage.itemThreeImageTopLandscape
import com.gapo.demoonboard.ui.newsfeed.models.gallery.threeimage.itemThreeImageTopPortrait
import com.gapo.demoonboard.ui.newsfeed.models.gallery.twoimage.itemTwoImageLandscape
import com.gapo.demoonboard.ui.newsfeed.models.gallery.twoimage.itemTwoImagePortrait
import com.gapo.demoonboard.ui.newsfeed.models.gallery.twoimage.itemTwoImageSquare
import com.gapo.demoonboard.utils.RecyclerViewUtils.Companion.getImageShapeType
import com.gapo.demoonboard.utils.RecyclerViewUtils.Companion.getOrientationForThreeImage
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.getRemainingImage
import com.gapo.domain.model.newsfeed.Image


class GalleryController(private val bag: DisposeBag) : TypedEpoxyController<List<Image>>() {

    override fun buildModels(data: List<Image>) {

        for (i in data.indices) {

            when (data.size) {
                1 -> {
                    buildItemViewForOneImage(data, i)
                }
                2 -> {
                    buildItemViewForSecondImage(data, i)
                }

                3 -> {
                    buildItemViewForThreeImage(data, i)
                }

                4 -> {
                    buildItemViewForFourImage(data, i)
                }

                5 -> {
                    buildItemViewForFiveImage(data, i)
                }
                else -> {

                    buildItemViewForMoreFiveImage(data, i)
                }
            }

        }

    }

    private fun buildItemViewForOneImage(data: List<Image>, position: Int) {
        when (getImageShapeType(data[position].width!!, data[position].height!!)) {
            ImageShapeType.PORTRAIT -> {
                itemOneImagePortrait {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }
            ImageShapeType.LANDSCAPE -> {
                itemOneImageLandscape {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }
            else -> {
                itemOneImageSquare {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }
        }
    }

    private fun buildItemViewForSecondImage(data: List<Image>, position: Int) {
        when (getImageShapeType(data)) {
            ImageShapeType.PORTRAIT -> {
                itemTwoImagePortrait {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }
            ImageShapeType.LANDSCAPE -> {
                itemTwoImageLandscape {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }
            else -> {
                itemTwoImageSquare {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }
        }
    }

    private fun buildItemViewForThreeImage(data: List<Image>, position: Int) {
        val getOrientationForThreeImage = getOrientationForThreeImage(data)
        if (position == 0) {
            if (getOrientationForThreeImage == GridLayoutManager.VERTICAL) {
                itemThreeImageTopLandscape {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            } else {
                itemThreeImageTopPortrait {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }

        } else {
            if (getOrientationForThreeImage == GridLayoutManager.VERTICAL) {
                itemThreeImage {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            } else {

                itemThreeImagePortrait {
                    id("gallery $position")
                    bag(bag)
                    imageGallery(data[position])
                    position((position))
                }
            }
        }
    }

    private fun buildItemViewForFourImage(data: List<Image>, position: Int) {
        itemFourImage {
            id("gallery $position")
            bag(bag)
            imageGallery(data[position])
            position((position))
        }
    }

    private fun buildItemViewForFiveImage(data: List<Image>, position: Int) {
        itemFiveImage {
            id("gallery $position")
            bag(bag)
            imageGallery(data[position])
            position((position))
        }
    }

    private fun buildItemViewForMoreFiveImage(data: List<Image>, position: Int) {
        if (position == 4) {
            itemMoreFiveImage {
                id("gallery $position")
                bag(bag)
                imageGallery(data[position])
                position((position))
                countRemaining(getRemainingImage(data.size, 4))
            }
        } else if (position < 4) {
            itemFiveImage {
                id("gallery $position")
                bag(bag)
                imageGallery(data[position])
                position((position))
            }
        }
    }
}
