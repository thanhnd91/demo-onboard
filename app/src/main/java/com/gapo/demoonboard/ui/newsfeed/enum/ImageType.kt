package com.gapo.demoonboard.ui.newsfeed.enum

enum class ImageType(val type: Int) {
    TYPE_ONE(1),
    TYPE_TWO(2),
    TYPE_THREE(3),
    TYPE_FOUR(4),
    TYPE_FIVE(5)
}