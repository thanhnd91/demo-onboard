package com.gapo.demoonboard.ui.home.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gapo.demoonboard.ui.newsfeed.NewsFeedFragment


class CategoryNewsFeedPagerAdapter(fa: FragmentActivity, var listCategory: List<String>) :
    FragmentStateAdapter(fa) {
    override fun getItemCount(): Int {
        return listCategory.size
    }

    override fun createFragment(position: Int): Fragment = NewsFeedFragment.getInstance()

}