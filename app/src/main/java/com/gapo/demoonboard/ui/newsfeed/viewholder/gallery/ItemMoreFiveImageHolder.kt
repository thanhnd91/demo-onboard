package com.gapo.demoonboard.ui.newsfeed.viewholder.gallery

import android.widget.ImageView
import android.widget.TextView

import com.gapo.demoonboard.R
import com.gapo.demoonboard.utils.epoxy.KotlinEpoxyHolder

class ItemMoreFiveImageHolder : KotlinEpoxyHolder() {
    val ivFive by bind<ImageView>(R.id.ivFive)
    val tvCount by bind<TextView>(R.id.tvCount)
}