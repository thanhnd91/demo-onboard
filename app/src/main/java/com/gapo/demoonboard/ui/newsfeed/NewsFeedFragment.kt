package com.gapo.demoonboard.ui.newsfeed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyVisibilityTracker
import com.gapo.demoonboard.R
import com.gapo.demoonboard.ui.newsfeed.controller.NewsFeedController
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.viewmodels.NewsFeedViewModel
import com.gapo.domain.model.newsfeed.ItemFeed
import kotlinx.android.synthetic.main.fragment_newsfeed.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class NewsFeedFragment() : Fragment() {

    private val bag = DisposeBag(this)
    private val viewModel by viewModel<NewsFeedViewModel>()

    companion object {
        fun getInstance() = NewsFeedFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_newsfeed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleCallback()
        getNewsFeed()
    }

    private fun getNewsFeed() {
        viewModel.getNewsFeed()
    }

    private fun handleCallback() {
        viewModel.newsFeedList.observe(::getLifecycle, ::updateUI)
    }

    private fun updateUI(itemFeedList: List<ItemFeed>) {
        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(rvNewsFeed)

        val controller = NewsFeedController(bag)
        controller.setData(itemFeedList)

        val layoutManager = LinearLayoutManager(context)
        rvNewsFeed.layoutManager = layoutManager

        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        context?.let {
            ContextCompat.getDrawable(
                it,
                R.drawable.divider
            )?.let { itemDecorator.setDrawable(it) }
        }

        rvNewsFeed.addItemDecoration(itemDecorator)

        rvNewsFeed.adapter = controller.adapter

        rvNewsFeed.setCacheManager(controller)
    }

}