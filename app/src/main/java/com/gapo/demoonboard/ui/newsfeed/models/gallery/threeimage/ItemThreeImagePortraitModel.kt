package com.gapo.demoonboard.ui.newsfeed.models.gallery.threeimage

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gapo.demoonboard.R
import com.gapo.demoonboard.di.GlideApp
import com.gapo.demoonboard.ui.newsfeed.enum.ImageType
import com.gapo.demoonboard.ui.newsfeed.viewholder.gallery.ItemThreeImagePortraitHolder
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.getPixelSizeFromDp
import com.gapo.demoonboard.utils.glide.GlideUtils.Companion.getRequestOptionsGalleryLinear
import com.gapo.demoonboard.utils.glide.MyImageRequestListener
import com.gapo.demoonboard.utils.widthScreen
import com.gapo.domain.model.newsfeed.Image


@EpoxyModelClass(layout = R.layout.item_three_image_portrait)
abstract class ItemThreeImagePortraitModel : EpoxyModelWithHolder<ItemThreeImagePortraitHolder>() {

    @EpoxyAttribute
    var imageGallery: Image? = null
    @EpoxyAttribute
    lateinit var bag: DisposeBag
    @EpoxyAttribute
    var position: Int = 0

    override fun bind(holder: ItemThreeImagePortraitHolder) {
        setImageSize(holder)
        loadImage(holder)
    }


    private fun loadImage(holder: ItemThreeImagePortraitHolder) {
        val requestOption = getRequestOptionsGalleryLinear(
            ImageType.TYPE_THREE.type,
            position
        )

        if (imageGallery != null)
            GlideApp.with(holder.ivThree.context).load(imageGallery?.href)
                .transition(DrawableTransitionOptions.withCrossFade())
                .thumbnail(
                    GlideApp.with(holder.ivThree.context)
                        .load(imageGallery?.href)
                        .apply(requestOption)
                )
                .apply(requestOption)
                .listener(MyImageRequestListener())
                .into(holder.ivThree)
    }

    private fun setImageSize(holder: ItemThreeImagePortraitHolder) {
        holder.ivThree.layoutParams.width =
            (widthScreen - getPixelSizeFromDp(21f, holder.ivThree.context)) / 2
        holder.ivThree.layoutParams.height =
            (widthScreen - getPixelSizeFromDp(21f, holder.ivThree.context)) / 2
        holder.ivThree.requestLayout()
    }
}


