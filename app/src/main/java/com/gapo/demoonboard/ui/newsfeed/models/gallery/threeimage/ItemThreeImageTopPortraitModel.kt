package com.gapo.demoonboard.ui.newsfeed.models.gallery.threeimage

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gapo.demoonboard.R
import com.gapo.demoonboard.di.GlideApp
import com.gapo.demoonboard.ui.newsfeed.enum.ImageType
import com.gapo.demoonboard.ui.newsfeed.viewholder.gallery.ItemThreeImageTopPortraitHolder
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.getPixelSizeFromDp
import com.gapo.demoonboard.utils.glide.GlideUtils
import com.gapo.demoonboard.utils.glide.MyImageRequestListener
import com.gapo.demoonboard.utils.widthScreen
import com.gapo.domain.model.newsfeed.Image


@EpoxyModelClass(layout = R.layout.item_three_image_top_portrait)
abstract class ItemThreeImageTopPortraitModel :
    EpoxyModelWithHolder<ItemThreeImageTopPortraitHolder>() {

    @EpoxyAttribute
    var imageGallery: Image? = null
    @EpoxyAttribute
    lateinit var bag: DisposeBag
    @EpoxyAttribute
    var position: Int = 0

    override fun bind(portraitHolder: ItemThreeImageTopPortraitHolder) {
        setImageSize(portraitHolder)
        loadImage(portraitHolder)
    }


    private fun loadImage(portraitHolder: ItemThreeImageTopPortraitHolder) {
        val requestOption = GlideUtils.getRequestOptionsGalleryLinear(
            ImageType.TYPE_THREE.type,
            position
        )

        if (imageGallery != null)
            GlideApp.with(portraitHolder.ivThreeTopPortrait.context).load(imageGallery?.href)
                .transition(DrawableTransitionOptions.withCrossFade())
                .thumbnail(
                    GlideApp.with(portraitHolder.ivThreeTopPortrait.context)
                        .load(imageGallery?.href)
                        .apply(requestOption)
                )
                .apply(requestOption)
                .listener(MyImageRequestListener())
                .into(portraitHolder.ivThreeTopPortrait)
    }

    private fun setImageSize(holder: ItemThreeImageTopPortraitHolder) {
        holder.ivThreeTopPortrait.layoutParams.width =
            (widthScreen - getPixelSizeFromDp(21f, holder.ivThreeTopPortrait.context)) / 2
        holder.ivThreeTopPortrait.layoutParams.height =
            widthScreen - getPixelSizeFromDp(20f, holder.ivThreeTopPortrait.context)
        holder.ivThreeTopPortrait.requestLayout()
    }
}


