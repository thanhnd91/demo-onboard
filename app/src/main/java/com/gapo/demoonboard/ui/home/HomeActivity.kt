package com.gapo.demoonboard.ui.home

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gapo.demoonboard.R
import com.gapo.demoonboard.ui.home.adapter.CategoryNewsFeedPagerAdapter
import com.gapo.demoonboard.utils.LocaleUtils.Companion.updateBaseContextLocale
import com.gapo.demoonboard.utils.getCategoryList
import com.gapo.demoonboard.utils.getWidthNewsFeedScreen
import com.gapo.demoonboard.utils.widthScreen
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private var categoryNewsFeedPagerAdapter: CategoryNewsFeedPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setUpView()
        setUpNewsFeedViewPager()
        widthScreen = getWidthNewsFeedScreen(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(updateBaseContextLocale(base))

    }

    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.navigation_home -> {
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_account -> {
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    private fun setUpView() {
        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    private fun setUpNewsFeedViewPager() {
        categoryNewsFeedPagerAdapter =
            CategoryNewsFeedPagerAdapter(this, getCategoryList(applicationContext))
        vpFeed.adapter = categoryNewsFeedPagerAdapter
        vpFeed.offscreenPageLimit = 1

        TabLayoutMediator(tlHeader, vpFeed) { tab, position ->
            tab.text = categoryNewsFeedPagerAdapter?.listCategory?.get(position)
        }.attach()
    }

}
