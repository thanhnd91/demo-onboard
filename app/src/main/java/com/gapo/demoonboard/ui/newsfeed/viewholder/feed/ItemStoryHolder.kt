package com.gapo.demoonboard.ui.newsfeed.viewholder.feed

import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.gapo.demoonboard.R
import com.gapo.demoonboard.utils.epoxy.KotlinEpoxyHolder

class ItemStoryHolder : KotlinEpoxyHolder() {
    val tvTitle by bind<TextView>(R.id.tvTitle)
    val tvPublisherInfo by bind<TextView>(R.id.tvPublisherInfo)
    val clClose by bind<ConstraintLayout>(R.id.clClose)
    val ivStory by bind<ImageView>(R.id.ivStory)

}