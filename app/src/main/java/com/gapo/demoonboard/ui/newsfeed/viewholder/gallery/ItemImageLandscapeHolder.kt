package com.gapo.demoonboard.ui.newsfeed.viewholder.gallery

import android.widget.ImageView
import com.gapo.demoonboard.R
import com.gapo.demoonboard.utils.epoxy.KotlinEpoxyHolder

class ItemImageLandscapeHolder : KotlinEpoxyHolder() {
    val ivLandscape by bind<ImageView>(R.id.ivLandscape)
}