package com.gapo.demoonboard.ui.newsfeed.models.feed

import android.net.Uri
import android.view.View
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gapo.demoonboard.R
import com.gapo.demoonboard.di.GlideApp
import com.gapo.demoonboard.ui.newsfeed.viewholder.feed.ItemVideoHolder
import com.gapo.demoonboard.utils.DELAY_TIME
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.disposebag.disposedBy
import com.gapo.demoonboard.utils.glide.GlideUtils.Companion.getRequestOptions
import com.gapo.demoonboard.utils.glide.MyImageRequestListener
import com.gapo.domain.model.newsfeed.Image
import com.jakewharton.rxbinding3.view.clicks
import java.util.concurrent.TimeUnit


@EpoxyModelClass(layout = R.layout.item_video)
abstract class ItemVideoModel : EpoxyModelWithHolder<ItemVideoHolder>() {

    @EpoxyAttribute
    lateinit var onCloseButtonClickListener: () -> Unit
    @EpoxyAttribute
    lateinit var title: String
    @EpoxyAttribute
    lateinit var publisherInfo: String
    @EpoxyAttribute
    lateinit var videoTime: String
    @EpoxyAttribute
    var imagePreView: Image? = null
    @EpoxyAttribute
    lateinit var bag: DisposeBag
    @EpoxyAttribute
    var href: String? = null

    override fun bind(holder: ItemVideoHolder) {
        holder.tvPublisherInfo.text = publisherInfo
        holder.tvTime.text = videoTime

        holder.parent.tag = holder
        handleClickEvent(holder)
        //loadImage(holder)
        holder.bindUri(0, Uri.parse(href))

        holder.tvTime.visibility == View.GONE
    }

    private fun handleClickEvent(holder: ItemVideoHolder) {
        holder.clClose.clicks()
            .throttleFirst(DELAY_TIME, TimeUnit.MILLISECONDS)
            .subscribe {
                onCloseButtonClickListener()
            }
            .disposedBy(bag)
    }

    private fun loadImage(holder: ItemVideoHolder) {
        val requestOption = getRequestOptions()
        if (imagePreView != null)
            GlideApp.with(holder.ivPreview.context).load(imagePreView?.href)
                .transition(DrawableTransitionOptions.withCrossFade())
                .thumbnail(
                    GlideApp.with(holder.ivPreview.context)
                        .load(imagePreView?.href)
                        .apply(requestOption)
                )
                .apply(requestOption)
                .listener(MyImageRequestListener())
                .into(holder.ivPreview)
    }
}


