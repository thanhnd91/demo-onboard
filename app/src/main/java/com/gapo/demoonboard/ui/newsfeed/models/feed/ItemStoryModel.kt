package com.gapo.demoonboard.ui.newsfeed.models.feed

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gapo.demoonboard.R
import com.gapo.demoonboard.di.GlideApp
import com.gapo.demoonboard.ui.newsfeed.viewholder.feed.ItemStoryHolder
import com.gapo.demoonboard.utils.DELAY_TIME
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.disposebag.disposedBy
import com.gapo.demoonboard.utils.glide.GlideUtils.Companion.getRequestOptions
import com.gapo.demoonboard.utils.glide.MyImageRequestListener
import com.gapo.domain.model.newsfeed.Image
import com.jakewharton.rxbinding3.view.clicks
import java.util.concurrent.TimeUnit


@EpoxyModelClass(layout = R.layout.item_story)
abstract class ItemStoryModel : EpoxyModelWithHolder<ItemStoryHolder>() {

    @EpoxyAttribute
    lateinit var onCloseButtonClickListener: () -> Unit
    @EpoxyAttribute
    lateinit var title: String
    @EpoxyAttribute
    lateinit var publisherInfo: String
    @EpoxyAttribute
    var imageStory: Image? = null
    @EpoxyAttribute
    lateinit var bag: DisposeBag

    override fun bind(holder: ItemStoryHolder) {
        holder.tvTitle.text = title
        holder.tvPublisherInfo.text = publisherInfo
        handleClickEvent(holder)
        loadImage(holder)
    }

    private fun handleClickEvent(holder: ItemStoryHolder) {
        holder.clClose.clicks()
            .throttleFirst(DELAY_TIME, TimeUnit.MILLISECONDS)
            .subscribe {
                onCloseButtonClickListener()
            }
            .disposedBy(bag)
    }

    private fun loadImage(holder: ItemStoryHolder) {
        val requestOption = getRequestOptions()

        if (imageStory != null) {
            GlideApp.with(holder.ivStory.context).load(imageStory?.href)
                .transition(DrawableTransitionOptions.withCrossFade())
                .thumbnail(
                    GlideApp.with(holder.ivStory.context)
                        .load(imageStory?.href)
                        .apply(requestOption)
                )
                .apply(requestOption)
                .listener(MyImageRequestListener())
                .into(holder.ivStory)
        } else
            GlideApp.with(holder.ivStory.context).load(R.drawable.placeholder).into(holder.ivStory)
    }

}


