package com.gapo.demoonboard.ui.newsfeed.enum

enum class ImageShapeType(val type: Int) {
    PORTRAIT(1),
    LANDSCAPE(2),
    SQUARE(3)
}