package com.gapo.demoonboard.ui.newsfeed.controller

import com.airbnb.epoxy.TypedEpoxyController
import com.airbnb.epoxy.toro.CacheManager
import com.gapo.demoonboard.ui.newsfeed.enum.ContentType
import com.gapo.demoonboard.ui.newsfeed.models.feed.itemGallery
import com.gapo.demoonboard.ui.newsfeed.models.feed.itemStory
import com.gapo.demoonboard.ui.newsfeed.models.feed.itemVideo
import com.gapo.demoonboard.utils.TimeUtils.Companion.getTimeVideo
import com.gapo.demoonboard.utils.disposebag.DisposeBag
import com.gapo.demoonboard.utils.getPublisherInfo
import com.gapo.demoonboard.utils.getRandomImage
import com.gapo.domain.model.newsfeed.ItemFeed


class NewsFeedController(private val bag: DisposeBag) : TypedEpoxyController<List<ItemFeed>>(),
    CacheManager {


    override fun buildModels(data: List<ItemFeed>) {
        dataSet.clear()
        dataSet.addAll(data)

        for (i in data.indices) {
            val itemFeed = data[i]
            when (itemFeed.contentType) {
                ContentType.GALLERY.type -> {
                    itemGallery {
                        id(itemFeed.documentId)
                        title(itemFeed.title)
                        publisherInfo(getPublisherInfo(itemFeed))
                        bag(bag)
                        itemFeed.images?.let { galleryList(it) }
                        onCloseButtonClickListener {
                        }
                    }
                }
                ContentType.VIDEO.type -> {
                    itemVideo {
                        id(itemFeed.documentId)
                        title(itemFeed.title)
                        publisherInfo(getPublisherInfo(itemFeed))
                        imagePreView(itemFeed.content?.previewImage)
                        bag(bag)
                        videoTime(getTimeVideo(itemFeed.content?.duration))
                        itemFeed.content?.href?.let { href(it) }
                        onCloseButtonClickListener {
                        }
                    }
                }

                else -> {
                    itemStory {
                        id(itemFeed.documentId)
                        title(itemFeed.title)
                        publisherInfo(getPublisherInfo(itemFeed))
                        itemFeed.images?.let { getRandomImage(it) }?.let { imageStory(it) }
                        bag(bag)
                        onCloseButtonClickListener {
                        }
                    }
                }
            }

        }
    }

    var dataSet = mutableListOf<Any>()

    override fun getKeyForOrder(order: Int): Any? {
        return dataSet[order]
    }

    override fun getOrderForKey(key: Any): Int? {
        return if (key is Any) dataSet.indexOf(key) else null
    }


}