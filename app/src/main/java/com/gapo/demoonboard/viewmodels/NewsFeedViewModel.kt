package com.gapo.demoonboard.viewmodels

import androidx.lifecycle.MutableLiveData
import com.gapo.demoonboard.viewmodels.base.BaseViewModel
import com.gapo.domain.model.newsfeed.ItemFeed
import com.gapo.domain.usecases.GetNewsFeedUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers


class NewsFeedViewModel(private val getNewsFeedUseCase: GetNewsFeedUseCase) : BaseViewModel() {
    val newsFeedList: MutableLiveData<List<ItemFeed>> = MutableLiveData()
    fun getNewsFeed() {

        getNewsFeedUseCase()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({listNewsFeed->
                this.newsFeedList.value = listNewsFeed
            }, {

            }).addTo(disposables)

    }

}


